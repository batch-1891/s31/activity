// 1. What directive is used by Node.js in loading the modules it needs?
// Write answers here... 
// http

// 2. What Node.js module contains a method for server creation?
 // port


// 3. What is the method of the http object responsible for creating a server using Node.js?
// createServer()


// 4. What method of the response object allows us to set status codes and content types?
// response.writeHead()    


// 5. Where will console.log() output its contents when run in Node.js?
// response.end()


// 6. What property of the request object contains the address's endpoint?
// server.listen()



const http = require('http')

const port = 3000

const server = http.createServer(function(request,response) {
	// You can use request.url to get the current destination of the user in the browser. You can then check if the current destination of the user matches with any endpoint and if it does, do the respective processes for each endpoint.
	if(request.url == '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to the login page.')
	} else if (request.url == '/register') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end(`I'm sorry the page you are looking for cannot be found.`)
	} else { //If none of the endpoints match the current destination of the user, return a default value like 'Page not available'
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('error.')
	}
})

server.listen(port)

console.log(`Server is successfully running.`)




